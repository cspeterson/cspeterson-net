# Prerequisites

* [Install Hugo]

# Run local server

```sh
hugo server -D
```

# Build site 

```sh
# Files end up in `./public`
hugo
```

# Deployment

Deployment to GitLab Pages is simple and is defined in `gitlab-ci.yml`.


[Install Hugo]: https://gohugo.io/getting-started/installing/
