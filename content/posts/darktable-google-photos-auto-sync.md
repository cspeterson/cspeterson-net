---
layout: post
date: 2021-03-08T09:38:16-05:00
title: 'Automatically importing Google Photos for darktable'
date: 
cover-img: /assets/tmux_change_directory.jpg
categories:
  - blog
tags:
  - bash
  - darktable
author: Christopher Peterson
slug: 'automatic-import-google-photos-darktable'
draft: true
---
