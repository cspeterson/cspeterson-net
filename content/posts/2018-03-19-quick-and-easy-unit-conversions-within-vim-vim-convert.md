---
layout: post
title: Quick and easy unit conversions within Vim (vim-convert)
date: 2018-03-19 21:11:58.000000000 -04:00
cover-img: /img/vim-convert.jpg
categories:
  - blog
tags:
- vim
author: Christopher Peterson
slug: '/quick-and-easy-unit-conversions-within-vim-vim-convert/'

---
Editing some varieties of documents I noticed myself jumping into a browser or terminal often to do unit conversion. But no longer! [Vim-convert](https://github.com/cspeterson/vim-convert) is a plugin I made for Vim that allows easy unit conversion from a simple command.

# The command

```viml
:Convert [[value]|[start unit]] {target unit}
```

# Some usage examples

```viml
" To convert in-place the currently visually-selected text which includes a unit, e.g. `500g`:
:Convert {target unit}

" To convert in-place the currently visually-selected text which does *not* include a unit, e.g. `500`, you'll need to specify one:
:Convert {start unit} {target unit}

" To insert a unit conversion as a new value after the cursor, give both fields:
:Convert <value><start unit> <target unit>

To cause an error because parsecs are a unit of distance:
:Convert 12parsecs hours
```

This plugin will try to be mindful of whether the unit itself is desired when replacing text. That is, if the selected text lacks a unit, the result will be placed without one as well.

Installation instructions and other details in the [README](https://github.com/cspeterson/vim-convert).


<sub><sup>
  Header image bits: <a href="https://commons.wikimedia.org/wiki/File:Vimlogo.svg">Vim logo from D0ktorz</a> and <a href="https://commons.wikimedia.org/wiki/File:SI_base_unit.svg">SI Units from Dono</a>
</sup></sub>
