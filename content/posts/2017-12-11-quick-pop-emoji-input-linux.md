---
layout: post
title: Quick pop-up emoji input on Linux [updated]
date: 2017-12-11 18:00:02.000000000 -05:00
cover-img: /img/linux-emoji-popup.jpg
categories:
  - blog
tags:
  - BASH
  - emoji
  - ibus
  - Linux
  - python
author: Christopher Peterson
slug: '/quick-pop-emoji-input-linux/'

---
## *UPDATE*: I've since rolled my own solution, which I prefer and recommend over this one. Check it out!

<div align="center">
<h2>✨<a href="/splatmoji-easier-quick-pop-up-emoji-emoticon-input-on-linux/">Splatmoji</a>✨</h2>
</div>

But the old post follows for reference..

I need pop-up Linux emoji input that doesn't require a mouse. Something like [Emoji Keyboard](https://github.com/OzymandiasTheGreat/emoji-keyboard) or [EmojiOne Picker](https://github.com/gentakojima/emojione-picker-ubuntu) require the hands to leave the keyboard and click on an emoji - this is a bunch  better than jumping into a web browser and looking one up, but still. Yuck.

The solution I finally found is [ibus-uniemoji](https://github.com/salty-horse/ibus-uniemoji). [Ibus](https://wiki.archlinux.org/index.php/IBus) is a framework on Unix that is most generally used for input of foreign characters from a standard keyboard, e.g. Pinyin, Russian, or in this case, emoji. Uniemoji uses this to provide an easy pop-up for emoji selection *by name or keyword*. No mouse!

![uniemoji emoji input](/img/uniemoji.gif)

```sh
# Ensure the IBus daemon is running at startup, or whenever suits you:
ibus-daemon -drx
git clone https://github.com/salty-horse/ibus-uniemoji /tmp/uniemoji
cd /tmp/uniemoji
sudo make install
ibus restart
```

Here is the script I wrote to toggle between normal input and emoji input:

```python
#!/usr/bin/env python3

# Given a list of possible ibus engines to cycle through as args, will cycle
# through them. If current engine is not in the list, will select the first
# from the args.

def main():
    """ main """
    import os
    import sys
    import subprocess

    engines = sys.argv[1::]
    if not engines:
        print('No engines given - nothing to do', file=sys.stderr)
        sys.exit(1)

    engine_current = subprocess.getoutput('ibus engine')

    curr_idx = engines.index(engine_current)
    next_idx = (curr_idx + 1) % len(engines)

    cmd = [
        'ibus',
        'engine',
        engines[next_idx],
    ]
    subprocess.run(cmd, check=True)
    cmd = [
        'notify-send',
        'Swtiched ibus engine to "{}"'.format(engines[next_idx]),
    ]
    subprocess.run(cmd)

if __name__ == '__main__':
    main()
```

And finally to toggle between (or cycle across) an arbitrary number of IBus engines, find the engines you want by running `ibus list-engine` and call the script like this from some key combination (see your X or window manager config, whatever):

```sh
ibus-switch.py engine1 engine2
```

In my case, I bound this via my [i3 config](https://github.com/cspeterson/dotfiles/blob/master/.config/i3/config):

```sh
ibus-swtich.py xkb:us::eng uniemoji && bash $HOME/.xsession
```
Note: you may, like me, need to re-establish your X keyboard settings after each time you switch IBus engines or else you'll lose your compose keys, maps, etc. To ensure this I just re-run my `.xsession` as above.
