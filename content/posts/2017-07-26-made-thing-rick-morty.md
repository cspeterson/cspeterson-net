---
layout: post
title: 'I made a thing: Memeseeks! (Rick & Morty memes and gifs) [UPDATED]'
date: 2017-07-26 12:42:25.000000000 -04:00
categories:
  - blog
tags:
  - code
  - Decipher Media
  - Memeseeks
  - programming
  - Rick and Morty
author: Christopher Peterson
slug: '/made-thing-rick-morty/'

---
future addedum: *A couple of days after this release, the people behind [Frinkiac](https://frinkiac.com/) released [their own site](https://masterofallscience.com/) that does the same thing as Memeseeks, but much nicer looking.* (︶︹︺)


*I'm a bit sad that I completed a thing and then saw it made obsolete two days later, but at least I probably learned a few things along the way. So I've retired Memeseeks, though I hope to modularize all of its functionality and release the code at some point.*

☹️

Just in time for the new season of Rick & Morty (as well as our own episode on the matter), we just released this thing we've been working on:

![The Memeseeks Box](/img/meeseeks-us.png)

It's a Rick & Morty memer and giffer (pronounced like the peanut butter?). We've been saying at [Decipher Scifi](https://decipherscifi.com) that we were working on a few things behind the scenes, and this was one of them. I'm glad to finally have it out there. :)

So meme away, and I hope you like it, and make sure you're [subscribed](https://decipherscifi.com/subscribe/) to the podcast for our Rick & Morty episode in a few days!

I may add some code stuff about this in the future.

PS: [Here](https://decipherscifi.com/rick-morty-feat-adrian-falcone-episode-101/) is our Rick & Morty episode of the podcast!
