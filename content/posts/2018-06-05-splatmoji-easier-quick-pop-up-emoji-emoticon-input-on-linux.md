---
layout: post
title: Splatmoji - easier quick pop-up emoji/emoticon input on Linux
date: 2018-06-05 20:52:22.000000000 -04:00
cover-img: /img/splatmoji.jpg
categories:
  - blog
tags:
  - BASH
  - dmenu
  - emoji
  - emoticons
  - programming
author: Christopher Peterson
slug: '/splatmoji-easier-quick-pop-up-emoji-emoticon-input-on-linux/'

---
I [previously posted]() about an emoji-input method using IBus that required jumping through some hoops and ultimately wasn't a very pretty UX.

But now I've rolled my own, and added Japanese emoticons/kaomoji as a bonus! I've titled it [Splatmoji](https://github.com/cspeterson/splatmoji), with respect to its wildcard nature for emotive text input.

<div align="center">
  ✨<a href="https://github.com/cspeterson/splatmoji">Splatmoji</a>✨
</div>

Quick start:

```sh
# sudo apt-get install rofi xdotool xsel || sudo yum install rofi xdotool xsel
git clone https://github.com/cspeterson/splatmoji.git
cd splatmoji

# Call with default emoji and emoticon data files, copy result to clipboard
./splatmoji copy

# Call with default emoji and emoticon data files, type out result
./splatmoji type

# Call with a specific custom data file, copy result to clipboard
./splatmoji copy /path/to/data/file1 /path/to/data/file2
```

Some suggestions for customizing its configuration, binding it to a key combo in your window manager, using your own emoticon sets, and other bits are in the [README](https://github.com/cspeterson/splatmoji).

![splatmoji animated usage example](/img/splatmoji-demo.gif)

<div align="center">
  ＼(^▽^＠)ノ
</div>
