---
layout: post
title: A faster LastPass lookup for your Linux desktop
date: 2017-11-25 13:49:20.000000000 -05:00
cover-img: /img/lastpassdmenu.jpg
categories:
  - blog
tags:
  - BASH
  - cli
  - dmenu
  - Firefox
  - LastPass
  - Linux
author: Christopher Peterson
slug: '/faster-lastpass-lookup-linux-desktop/'
---
At work we noticed how the [LastPass plugin](https://addons.mozilla.org/en-US/firefox/addon/lastpass-password-manager/) on the new [Firefox Quantum](https://blog.mozilla.org/blog/2017/11/14/introducing-firefox-quantum/) (due to limitations of the new plugin architecture?) no longer has copy a "Copy Password" button. LastPass support suggested that this might not be changing anytime soon. We rely on this a lot for different terminal-based work, so it was a sad revelation!

This bothered me enough that I made my own LastPass pop up thing for my Linux desktop and as a bonus the workflow is also much faster than it was in the browser.

<div align="center">
✨<a href="https://github.com/cspeterson/lastpass-dmenu">Lastpass-dmenu</a>✨
</div>

<br>
![LastPass dmenu desktop pop up menu gif](/img/lastpass-dmenu.gif)

Here is exactly what install could look like on Ubuntu, for example

```sh
# Lastpass cli is the lastpass cli client from LastPass themselves
sudo apt-get install dmenu lastpass-cli
mkdir $HOME/.bin
wget -O $HOME/.bin/lastpass-cli https://raw.githubusercontent.com/cspeterson/lastpass-dmenu/master/lastpass-dmenu
chmod +x $HOME/.bin/lastpass-cli
# Login to lastpass-cli one time and it will remember your email for the
# future
lpass login myuser@domain.tld
```

And then run it there(no) or bind it to a hotkey(yes) in your window manager/whatever (I think this is under Keyboard settings in e.g. Gnome?).

When the menu pops up, just start typing the entry you want. It searches through both entry Name and Username. Whichever entry you select in the dmenu popup, the corresponding password will drop into your clipboard. Easy peasy.

# Security

By default as of this writing, `lpass` seems to just leave your password in the X primary clipboard forever (or until overwritten). It recognizes an environment variable however, `LPASS_CLIPBOARD_COMMAND`, where you can specify your clipboard command and arguments. This allows for a setting like the following:

```sh
export LPASS_CLIPBOARD_COMMAND="xclip -selection clipboard -in -l 1"
```
Which will allow one X selection request (i.e. a paste action) before this value is cleared from the clipboard.

See this [issue](https://github.com/lastpass/lastpass-cli/issues/59) and realize that it seems the environment variable trickery above will remain the only solution, so get used to it ;-)
