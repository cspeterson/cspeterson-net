---
author: Christopher Peterson
categories:
  - blog
date: 2017-08-04 09:36:38.000000000 -04:00
hide: true
layout: post
noindex: true
slug: '/puppet-skeleton-profile/'
type: post
tags:
  - code
  - puppet
  - roles and profiles
title: My Puppet skeleton profile

---
<meta name="robots" content="noindex">
<meta http-equiv="refresh" content="0; url=https://example.com/posts/my-intended-url"/>
