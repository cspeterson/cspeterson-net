---
layout: post
title: Using CSSH/XTerm with a high-DPI display
date: 2017-10-02 14:02:22.000000000 -04:00
cover-img: /img/hidpi-cssh-xterm.jpg
categories:
  - blog
tags:
  - configuration
  - cssh
  - Linux
  - ssh
  - terminal
author: Christopher Peterson
slug: '/using-csshxterm-high-dpi-display/'

---
On my laptop with a [QuadHD](https://en.wikipedia.org/wiki/Graphics_display_resolution#Quad_Extended_Graphics_Array) display, [ClusterSSH](https://github.com/duncs/clusterssh) was unusable.

![Screenshot of cssh using tiny bitmapped fonts](/img/cssh-screenshot-1.png)

... Because X is a hundred years old and xterm (the default for cssh) defaults to bitmapped fonts which are tiny and gross.

So how do we get them it to look like this instead?

![Screenshot of cssh using xterm with light/colored terminals at a legible size](/img/cssh-screenshot-2.png)

# Solution

This worked for me: just configure XTerm to use TrueType fonts!

```
! Drop this or something like it into
! ~/.Xresources

! to enable any changes to this configuration:
! xrdb -merge ~/.Xresources

! Set front to truetype as the bitmap-based default fonts are dumb and
! tiny on high-DPI displays. Also wtf who wants bitmapped fonts
XTerm*faceName: DejaVu Sans Mono
XTerm*faceSize: 8
XTerm*renderFont: true
```

And then configure cssh to a more appropriate size!

```
# Drop this or something like it into
# ~/.clusterssh/config

terminal_bg_style=dark
terminal_colorize=1
terminal_reserve_top=10
terminal_size=160x48
```

There you go.  That's so much nicer.

![Screenshot of cssh using xterm with light/colored terminals at a legible size](/img/cssh-screenshot-2.png)
